# PHP Unzip #
A long time ago I used to use a free webhost that placed a limit on the number of files that could be uploaded using PHP, and I needed a way to deploy a larger batch of files to the server.

I found a script online (original source unknown, sorry) and extensively adapted it to fit my needs. The script first unzips a .zip archive into a folder corresponding exactly to the archive's filename, then deletes the source archive file, and finally deletes itself.

Even if you don't have a limit on the number of files you can upload to your webhost like I did, this script may still be useful: uploading a single 25mb .zip archive to your host and then unzipping it once it's there is almost certainly faster than uploading a thousand smaller files that total 25mb.

## Usage ##
1. Upload the `unzip.php` script along with the zipped archive you'd like to extract into a folder of your choice.
2. Once it's uploaded, open a browser and navigate to the `unzip.php` script, adding the archive filename as an attribute in the URL named `src`. *E.g. http://foo.com/unzip.php?src=stuff.zip*
3. The files within *stuff.zip* will be extracted to *http://foo.com/stuff/*, and any directory structure that existed within the *stuff.zip* archive is retained.
4. Both the original archive file, *stuff.zip*, and the tool itself, *unzip.php*, are deleted at the end of the process.

Enjoy!