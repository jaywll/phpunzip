<?php
   if (!isset($_GET['src'])) echo "Usage: unzip.php?src=<em><strong>file.zip</strong></em>";
   else {
      unzipnew($_GET['src']);
      unlink("unzip.php");
   }

   function unzipnew($src_file, $dest_dir=false, $create_zip_name_dir=true, $overwrite=true, $delzip=true) {
      $filecounter = 0;
      echo "<p>Writing files (a count of files written will be at the bottom the page)...</p>";
      if ($zip = zip_open($src_file)) {
         if ($zip) {
            $splitter = ($create_zip_name_dir === true) ? "." : "/";
            if ($dest_dir === false) $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter))."/";
            create_dirs($dest_dir);
            while ($zip_entry = zip_read($zip)) {
               $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
               if ($pos_last_slash !== false) {
                  create_dirs($dest_dir.substr(zip_entry_name($zip_entry), 0, $pos_last_slash+1));
               }
               if (zip_entry_open($zip,$zip_entry,"r")) {
                  $file_name = $dest_dir.zip_entry_name($zip_entry);
                  if ($overwrite === true || $overwrite === false && !is_file($file_name)) {
                     $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                     @file_put_contents($file_name, $fstream );
                     chmod($file_name, 0777);
                     echo "File written: ".$file_name."<br />";
                     $filecounter++;
                  }
                  zip_entry_close($zip_entry);
               }
            }
            zip_close($zip);
            if($delzip) unlink($src_file);
            echo "<p>".$filecounter." files written.</p>";
         }
      } else {
         return false;
      }
      return true;
   }

   function create_dirs($path) {
      if (!is_dir($path)) {
         $directory_path = "";
         $directories = explode("/",$path);
         array_pop($directories);
         foreach($directories as $directory) {
            $directory_path .= $directory."/";
            if (!is_dir($directory_path)) {
               mkdir($directory_path);
               @chmod($directory_path, 0777);
            }
         }
      }
   }
?>